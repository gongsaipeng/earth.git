import Vue from "vue"
// 立即执行函数,这样可以在mian函数直接引入文件不需要写而外的代码
(function () {
    Vue.directive("permission", {
        inserted: function (el, binding) {
            console.log(el,"elllllllllllllll");
            // 获取用户权限 [每个人存放用户权限的位置不一致，需要根据自己的业务进行相应的修改]
            // let user_permissions = localStorage.getItem('permissions') || [];
            // let user_permissions = getPermission("Permission")
            //     .substring(1, getPermission("Permission").length - 1)
            //     .split(",") || [];
            let user_permissions=["red","blue"]
            // 获取指令中配置的权限
            let permissions = [];
            var type = Object.prototype.toString.call(binding.value);
            switch (type) {
                case '[object Array]':
                    permissions = binding.value;
                    break;
                case '[object Number]':
                    permissions.push(binding.value);
                    break;
                default:
                    permissions = binding.value.split('|');
                    break;
            }
            // 配置默认 admin 可以范围所有,这里可以结合你的业务进行调整  
            // permissions.push(1);
            // 默认不隐藏
            let flag = false;
            if (user_permissions.length == 0) flag = true;
            else {
                // console.log([...new Set(permissions)], "div所有要的权限");
                // console.log(user_permissions, "用户权限");
                if (user_permissions.length == 0)
                    flag = true;
                else {
                    flag = [...new Set(permissions)].filter(item => user_permissions.indexOf(item) >= 0).length > 0;
                }
            }
            // 用户权限不足则隐藏
            if (!flag) {
                // el.parentNode && el.parentNode.removeChild(el);//删除元素
                console.log(el.style, "!!!!!!!!!!!!!!");
                // el.style.display = "none"//隐藏元素
                el.disabled = true//el-button禁止元素
                // el.style.cursor = 'not-allowed'//获取焦点的小手，这里如果直接使用的话，禁用效果会被覆盖掉
                el.style.pointerEvents = 'none'//div禁止元素
                /**
                 * 使用这种方法的话必须在使用的时候给添加权限的div组件在添加一个相同大小的父组件才可以使用
                 * 否则会出现样式紊乱的结果
                 */
                el.parentNode.style.cursor = 'not-allowed'//给父级添加禁用小手的样式
            }
        }
    });
})();
